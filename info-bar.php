<div class="info">
	<p class="head"><a href="http://www.zenziva.net" title="Zenziva" target="_blank"><strong class="zenziva">ZENZIVA SMS</strong></a></p>
  <div>
    <p>Kirim SMS Notifikasi ke pembeli jika ada perubahan status untuk barang yang diordernya. Juga anda sebagai admin akan menerima SMS Notifikasi jika ada order baru.</p>
    <p>Anda bisa mengkostumisasi isi SMS Notifikasi yang akan dikirim dengan menambahkan variabel-variabel.</p>
    <p><strong>Variable yang dapat dimasukkan:</strong><br />
    %id%, %product%, %billing_first_name%, %billing_last_name%, %billing_email%, %billing_phone%, %shipping_first_name%, %shipping_last_name%, %shipping_company%, %shipping_address_1%, %shipping_address_2%, %shipping_city%, %shipping_postcode%, %shipping_country%, %shipping_state%, %shipping_method%, %shipping_method_title%, %payment_method%, %payment_method_title%, %order_total%, %status%, %shop_name% dan %note%.</p>
    
  </div>
  <div>
  	<p>
    	<div class="parent">
			 	<div class="left"><a href="https://twitter.com/zenziva" title="Follow us on Twitter" target="_blank"><span class="icon-social19"></span></a></div>
			 	<div class="right"><a href="mailto:support@zenziva.com" title="Contact with us by e-mail"><span class="icon-open21"></span></a></div>
			 	<div class="left"><a href="http://zenziva.net/kontak/" title="Documentation and Support" target="_blank"><span class="icon-work"></span></a></div>
			</div>
    </p>
  </div>
</div>
