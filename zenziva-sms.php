<?php
/*
Plugin Name: WooCommerce - Zenziva SMS
Version: 1.0
Plugin URI: http://zenziva.net
Description: Add SMS notifications in WooCommerce to your clients for order status changes. Also you can receive an SMS message when the shop get a new order.
Author URI: http://www.zenziva.net
Author: Zenziva

Text Domain: zenziva_sms
Domain Path: /lang
License: GPL2
*/

$zenziva_sms = array(	'plugin' => 'WooCommerce - Zenziva SMS',
					'plugin_uri' => 'woocommerce-zenziva-sms',
					'plugin_url' => 'http://zenziva.net',
					'paypal' => 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=J3RA5W3U43JTE',
					'ajustes' => 'admin.php?page=zenziva_sms');

// language
load_plugin_textdomain('zenziva_sms', null, dirname(plugin_basename(__FILE__)) . '/lang');

// Setting url in Plugin
function zenziva_sms_setting_url($url) {
	global $zenziva_sms;

	$setting_url = '<a href="' . $zenziva_sms['ajustes'] . '">' . __('Settings', 'zenziva_sms') . '</a>';
	array_unshift($url, $setting_url);

	return $url;
}
$plugin = plugin_basename(__FILE__);
add_filter("plugin_action_links_$plugin", 'zenziva_sms_setting_url');

// include sms setting page
function zenziva_sms_tab() {
	wp_enqueue_style('zenziva_sms_css');
	include('sms-settings.php');
}

// buat submenu pada menu admin woocommerce
function zenziva_sms_admin_menu() {
	add_submenu_page('woocommerce', __('Zenziva SMS', 'zenziva_sms'),  __('Zenziva SMS', 'zenziva_sms') , 'manage_woocommerce', 'zenziva_sms', 'zenziva_sms_tab');
}
add_action('admin_menu', 'zenziva_sms_admin_menu', 15);

// Load scripts and WooCommerce CSS
function zenziva_sms_screen_id($woocommerce_screen_ids) {
	global $woocommerce;

	$woocommerce_screen_ids[] = 'woocommerce_page_zenziva_sms';

	return $woocommerce_screen_ids;
}
add_filter('woocommerce_screen_ids', 'zenziva_sms_screen_id');

// Tambahkan pilihan
function zenziva_sms_register_options() {
	register_setting('zenziva_sms_settings_group', 'zenziva_sms_settings');
}
add_action('admin_init', 'zenziva_sms_register_options');

// Proses Customer SMS
function zenziva_sms_process_states($order) {
	global $woocommerce;

	$order = new WC_Order($order);
	$status = $order->status;
	$status_list = array(
												'pending' => __('Pending', 'zenziva_sms'),
												'failed' => __('Failed', 'zenziva_sms'),
												'on-hold' => 'Receive',
												'processing' => __('Processing', 'zenziva_sms'),
												'completed' => __('Completed', 'zenziva_sms'),
												'refunded' => __('Refunded', 'zenziva_sms'),
												'cancelled' => __('Cancelled', 'zenziva_sms')
											);
	foreach ($status_list as $status_lists => $translations) if ($status == $status_lists) $status = $translations;

	$config = get_option('zenziva_sms_settings');

	$internacional = false;
	$phone = zenziva_sms_set_phone($order, $order->billing_phone, $config['gateway']);
	if ($order->billing_country && ($woocommerce->countries->get_base_country() != $order->billing_country)) $internacional = true;

	if ($status == 'Receive') $msg = zenziva_sms_process_variables($config['customer_neworder'], $order, $config['variables']);
	else if ($status == __('Pending', 'zenziva_sms')) $msg = zenziva_sms_process_variables($config['order_pending'], $order, $config['variables']);
	else if ($status == __('Failed', 'zenziva_sms')) $msg = zenziva_sms_process_variables($config['order_failed'], $order, $config['variables']);
	else if ($status == __('Processing', 'zenziva_sms')) $msg = zenziva_sms_process_variables($config['order_processing'], $order, $config['variables']);
	else if ($status == __('Completed', 'zenziva_sms')) $msg = zenziva_sms_process_variables($config['order_completed'], $order, $config['variables']);
	else if ($status == __('Refunded', 'zenziva_sms')) $msg = zenziva_sms_process_variables($config['order_refunded'], $order, $config['variables']);
	else if ($status == __('Cancelled', 'zenziva_sms')) $msg = zenziva_sms_process_variables($config['order_cancelled'], $order, $config['variables']);

	if(!empty($msg)) zenziva_sms_send_sms($config, $phone, $msg);
}
/* action command when woocoomerce order firing */
add_action('woocommerce_order_status_pending', 'zenziva_sms_process_states', 10);
//add_action('woocommerce_order_status_failed', 'zenziva_sms_process_states', 10);
//add_action('woocommerce_order_status_on-hold', 'zenziva_sms_process_states', 10);
add_action('woocommerce_order_status_completed', 'zenziva_sms_process_states', 10);
add_action('woocommerce_order_status_processing', 'zenziva_sms_process_states', 10);
//add_action('woocommerce_order_status_refunded', 'zenziva_sms_process_states', 10);
//add_action('woocommerce_order_status_cancelled', 'zenziva_sms_process_states', 10);

// Proses Admin SMS
function zenziva_sms_process_order($order) {
	global $woocommerce;

	$order = new WC_Order($order);
	$config = get_option('zenziva_sms_settings');

	if (!empty($config['owner_neworder'])) zenziva_sms_send_sms($config, $config['admin_phone'], zenziva_sms_process_variables($config['owner_neworder'], $order, $config['variables']));
}
// Monitor changes in orders
add_action('woocommerce_order_status_pending_to_processing_notification', 'zenziva_sms_process_order', 10);
add_action('woocommerce_order_status_pending_to_on-hold_notification', 'zenziva_sms_process_order', 10);
add_action('woocommerce_order_status_pending_to_completed_notification', 'zenziva_sms_process_order', 10);

// Send note
function zenziva_sms_process_note($data) {
	global $woocommerce;

	$order = new WC_Order($data['order_id']);

	$config = get_option('zenziva_sms_settings');

	$internacional = false;
	$phone = zenziva_sms_set_phone($order, $order->billing_phone, $config['gateway']);
	if ($order->billing_country && ($woocommerce->countries->get_base_country() != $order->billing_country)) $internacional = true;

	zenziva_sms_send_sms($config, $phone, zenziva_sms_process_variables($config['order_note'], $order, $config['variables'], wptexturize($data['customer_note'])));
}
add_action('woocommerce_new_customer_note', 'zenziva_sms_process_note', 10);

// Send SMS
function zenziva_sms_send_sms($config, $phone, $msg) {
	if ($config['gateway'] == "reguler"){

		$userkey = $config['userkey_zenziva'];
		$passkey = $config['passkey_zenziva'];
		$url = "http://reguler.zenziva.net/apps/smsapi.php";
		mb_internal_encoding("UTF-8");
	  mb_http_output("UTF-8");
		$text = urlencode($message);
		$content = 'userkey='.rawurlencode($userkey).
				       '&passkey='.rawurlencode($passkey).
				       '&nohp='.rawurlencode($phone).
				       '&pesan='.zenziva_sms_encoding($msg);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$getresponse = curl_exec($ch);
		curl_close($ch);
		$xmldata = new SimpleXMLElement($getresponse);
		$status = $xmldata->message[0]->text;
	}elseif ($config['gateway'] == "center"){

		$userkey = $config['userkey_zenziva'];
		$passkey = $config['passkey_zenziva'];
		$url = "http://".$config['subdomain_zenziva'].".zenziva.co.id/api/sendsms/";
		mb_internal_encoding("UTF-8");
	  mb_http_output("UTF-8");
		$text = urlencode($message);
		$content = 'userkey='.rawurlencode($userkey).
				       '&passkey='.rawurlencode($passkey).
				       '&nohp='.rawurlencode($phone).
				       '&tipe=reguler'.
				       '&pesan='.zenziva_sms_encoding($msg);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$getresponse = curl_exec($ch);
		curl_close($ch);
		$xmldata = new SimpleXMLElement($getresponse);
		$status = $xmldata->message[0]->text;
	}elseif ($config['gateway'] == "masking"){

		$userkey = $config['userkey_zenziva'];
		$passkey = $config['passkey_zenziva'];
		$url = "http://alpha.zenziva.net/apps/smsapi.php";
		mb_internal_encoding("UTF-8");
	  mb_http_output("UTF-8");
		$text = urlencode($message);
		$content = 'userkey='.rawurlencode($userkey).
				       '&passkey='.rawurlencode($passkey).
				       '&nohp='.rawurlencode($phone).
				       '&pesan='.zenziva_sms_encoding($msg);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$getresponse = curl_exec($ch);
		curl_close($ch);
		$xmldata = new SimpleXMLElement($getresponse);
		$status = $xmldata->message[0]->text;
	}else{
		$status = "";
	}
}

// Excecute curl
function zenziva_sms_curl($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
	$results = curl_exec ($ch);
	curl_close($ch);

	return utf8_encode($results);
}

// Normalize the text
function zenziva_sms_normalize_message($msg)
{
	$replacement = array('Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e',  'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y',  'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', "`" => "'", "´" => "'", "„" => ",", "`" => "'", "´" => "'", "“" => "\"", "”" => "\"", "´" => "'", "&acirc;€™" => "'", "{" => "", "~" => "", "–" => "-", "’" => "'", "!" => ".", "¡" => "", "?" => ".", "¿" => "");

	$msg = str_replace(array_keys($replacement), array_values($replacement), htmlentities($msg, ENT_QUOTES, "UTF-8"));

	return $msg;
}

// Encoding message
function zenziva_sms_encoding($msg) {
	return urlencode(htmlentities($msg, ENT_QUOTES, "UTF-8"));
}

// See if you need international phone code
function zenziva_sms_prefix($gateway) {
	if ($gateway == "clockwork" || $gateway == "clickatell" || $gateway == "bulksms" || $gateway == "msg91" || $gateway == "twillio") return true;

	return false;
}

// Processes the phone and added, if needed, the prefix
function zenziva_sms_set_phone($order, $phone, $gateway) {
	global $woocommerce;

	$prefix = zenziva_sms_prefix($gateway);

	$phone = str_replace(array('+','-'), '', filter_var($phone, FILTER_SANITIZE_NUMBER_INT));
	if ($order->billing_country && ($woocommerce->countries->get_base_country() != $order->billing_country || $prefix))
	{
		$prefix_international = prefix($order->billing_country);
		preg_match("/(\d{1,4})[0-9.\- ]+/", $phone, $prefix);

		if (strpos($prefix[1], $prefix_international) === false){
			if (substr($phone, 0, 2) == '00') {
				$phone = $prefix_international . substr($phone, 2, strlen($phone));
			}else
			if (substr($phone, 0, 1) == '0') {
				return $prefix_international . substr($phone, 1, strlen($phone));
			}else
			if (substr($phone, 0, 1) == '+') {
				return substr($phone, 1, strlen($phone));
			}else{
				$phone = $prefix_international . $phone;
			}
		}
		if ($gateway == "twillio") $phone = "+" . $phone;
	}

	return $phone;
}

// Process variables
function zenziva_sms_process_variables($msg, $order, $variables, $note = '') {
	global $wpdb;
	$zenziva_sms = array(
									"id",
									"order_key",
									"billing_first_name",
									"billing_last_name",
									"billing_company",
									"billing_address_1",
									"billing_address_2",
									"billing_city",
									"billing_postcode",
									"billing_country",
									"billing_state",
									"billing_email",
									"billing_phone",
									"shipping_first_name",
									"shipping_last_name",
									"shipping_company",
									"shipping_address_1",
									"shipping_address_2",
									"shipping_city",
									"shipping_postcode",
									"shipping_country",
									"shipping_state",
									"shipping_method",
									"shipping_method_title",
									"payment_method",
									"payment_method_title",
									"order_subtotal",
									"order_discount",
									"cart_discount",
									"order_tax",
									"order_shipping",
									"order_shipping_tax",
									"order_total",
									"status",
									"shop_name",
									"note",
									"jne_tracking",
									"tracking_provider",
									"custom_tracking_provider",
									"tracking_number",
									"custom_tracking_link",
									"date_shipped",
									"product"
								);

	$variables = str_replace(array("\r\n", "\r"), "\n", $variables);
	$variables = explode("\n", $variables);

	preg_match_all("/%(.*?)%/", $msg, $search);

	$date_shipped = get_post_meta($order->id, '_date_shipped', true);
	$shiping_date = $date_shipped ? date('Y-m-d', $date_shipped) : '';

	foreach ($search[1] as $variable)
	{
    $variable = strtolower($variable);

    if (!in_array($variable, $zenziva_sms) && !in_array($variable, $variables)) continue;

    if ($variable != "shop_name" && $variable != "note" && $variable != "jne_tracking" && $variable != "tracking_provider" && $variable != "custom_tracking_provider" && $variable != "tracking_number" && $variable != "custom_tracking_link" && $variable != "date_shipped" && $variable != "product")
		{
			if (in_array($variable, $zenziva_sms)) $msg = str_replace("%" . $variable . "%", $order->$variable, $msg); // Variables standars
			else $msg = str_replace("%" . $variable . "%", $order->order_custom_fields[$variable][0], $msg); // Variables additional
		}
		else if ($variable == "shop_name") $msg = str_replace("%" . $variable . "%", get_bloginfo('name'), $msg);
		else if ($variable == "note") $msg = str_replace("%" . $variable . "%", $note, $msg);

		// if already WooCommerce JNE Shipping plugin
		else if ($variable == "jne_tracking") $msg = str_replace("%" . $variable . "%", get_post_meta($order->id, 'tracking', true), $msg);

		// if already WooCommerce Shipment Tracking plugin
		else if ($variable == "tracking_provider") $msg = str_replace("%" . $variable . "%", get_post_meta($order->id, '_tracking_provider', true), $msg);
		else if ($variable == "custom_tracking_provider") $msg = str_replace("%" . $variable . "%", get_post_meta($order->id, '_custom_tracking_provider', true), $msg);
		else if ($variable == "tracking_number") $msg = str_replace("%" . $variable . "%", get_post_meta($order->id, '_tracking_number', true), $msg);
		else if ($variable == "custom_tracking_link") $msg = str_replace("%" . $variable . "%", get_post_meta($order->id, '_custom_tracking_link', true), $msg);
		else if ($variable == "date_shipped") $msg = str_replace("%" . $variable . "%", $shiping_date, $msg);
		else if ($variable == "product") {
			$product_items = '';
			$count = $wpdb->get_var("SELECT COUNT(order_item_id) FROM {$wpdb->prefix}woocommerce_order_items WHERE order_id=".$order->id." AND order_item_type='line_item'");
			if ($count > 1) {
				$new_line = "\n";
				$no = 1;
			} else {
				$new_line = "";
				$no = "";
			}
			$product_query = $wpdb->get_results("SELECT order_item_id FROM {$wpdb->prefix}woocommerce_order_items WHERE order_id=".$order->id." AND order_item_type='line_item'");
			foreach ($product_query as $products) {
				$id_product = $wpdb->get_var("SELECT `meta_value` FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE `order_item_id`='".$products->order_item_id."' AND meta_key='_product_id'");
				$nama_product = $wpdb->get_var("SELECT `post_title` FROM {$wpdb->prefix}posts WHERE `ID`='".$id_product."'");
				$qty = $wpdb->get_var("SELECT `meta_value` FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE `order_item_id`='".$products->order_item_id."' AND meta_key='_qty'");
				$total = $wpdb->get_var("SELECT `meta_value` FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE `order_item_id`='".$products->order_item_id."' AND meta_key='_line_total'");
				$product_items .= $new_line . $no . ". " . $nama_product . ' x ' . $qty . ' = ' . $total;
				$count > 1 ? $no++ : '';
			}
			$msg = str_replace("%" . $variable . "%", $product_items, $msg);
		}
	}

	return $msg;
}

// Returns the country code prefix
function prefix($country = '') {
	$countries = array('AC' => '247', 'AD' => '376', 'AE' => '971', 'AF' => '93', 'AG' => '1268', 'AI' => '1264', 'AL' => '355', 'AM' => '374', 'AO' => '244', 'AQ' => '672', 'AR' => '54', 'AS' => '1684', 'AT' => '43', 'AU' => '61', 'AW' => '297', 'AX' => '358', 'AZ' => '994', 'BA' => '387', 'BB' => '1246', 'BD' => '880', 'BE' => '32', 'BF' => '226', 'BG' => '359', 'BH' => '973', 'BI' => '257', 'BJ' => '229', 'BL' => '590', 'BM' => '1441', 'BN' => '673', 'BO' => '591', 'BQ' => '599', 'BR' => '55', 'BS' => '1242', 'BT' => '975', 'BW' => '267', 'BY' => '375', 'BZ' => '501', 'CA' => '1', 'CC' => '61', 'CD' => '243', 'CF' => '236', 'CG' => '242', 'CH' => '41', 'CI' => '225', 'CK' => '682', 'CL' => '56', 'CM' => '237', 'CN' => '86', 'CO' => '57', 'CR' => '506', 'CU' => '53', 'CV' => '238', 'CW' => '599', 'CX' => '61', 'CY' => '357', 'CZ' => '420', 'DE' => '49', 'DJ' => '253', 'DK' => '45', 'DM' => '1767', 'DO' => '1809', 'DO' => '1829', 'DO' => '1849', 'DZ' => '213', 'EC' => '593', 'EE' => '372', 'EG' => '20', 'EH' => '212', 'ER' => '291', 'ES' => '34', 'ET' => '251', 'EU' => '388', 'FI' => '358', 'FJ' => '679', 'FK' => '500', 'FM' => '691', 'FO' => '298', 'FR' => '33', 'GA' => '241', 'GB' => '44', 'GD' => '1473', 'GE' => '995', 'GF' => '594', 'GG' => '44', 'GH' => '233', 'GI' => '350', 'GL' => '299', 'GM' => '220', 'GN' => '224', 'GP' => '590', 'GQ' => '240', 'GR' => '30', 'GT' => '502', 'GU' => '1671', 'GW' => '245', 'GY' => '592', 'HK' => '852', 'HN' => '504', 'HR' => '385', 'HT' => '509', 'HU' => '36', 'ID' => '62', 'IE' => '353', 'IL' => '972', 'IM' => '44', 'IN' => '91', 'IO' => '246', 'IQ' => '964', 'IR' => '98', 'IS' => '354', 'IT' => '39', 'JE' => '44', 'JM' => '1876', 'JO' => '962', 'JP' => '81', 'KE' => '254', 'KG' => '996', 'KH' => '855', 'KI' => '686', 'KM' => '269', 'KN' => '1869', 'KP' => '850', 'KR' => '82', 'KW' => '965', 'KY' => '1345', 'KZ' => '7', 'LA' => '856', 'LB' => '961', 'LC' => '1758', 'LI' => '423', 'LK' => '94', 'LR' => '231', 'LS' => '266', 'LT' => '370', 'LU' => '352', 'LV' => '371', 'LY' => '218', 'MA' => '212', 'MC' => '377', 'MD' => '373', 'ME' => '382', 'MF' => '590', 'MG' => '261', 'MH' => '692', 'MK' => '389', 'ML' => '223', 'MM' => '95', 'MN' => '976', 'MO' => '853', 'MP' => '1670', 'MQ' => '596', 'MR' => '222', 'MS' => '1664', 'MT' => '356', 'MU' => '230', 'MV' => '960', 'MW' => '265', 'MX' => '52', 'MY' => '60', 'MZ' => '258', 'NA' => '264', 'NC' => '687', 'NE' => '227', 'NF' => '672', 'NG' => '234', 'NI' => '505', 'NL' => '31', 'NO' => '47', 'NP' => '977', 'NR' => '674', 'NU' => '683', 'NZ' => '64', 'OM' => '968', 'PA' => '507', 'PE' => '51', 'PF' => '689', 'PG' => '675', 'PH' => '63', 'PK' => '92', 'PL' => '48', 'PM' => '508', 'PR' => '1787', 'PR' => '1939', 'PS' => '970', 'PT' => '351', 'PW' => '680', 'PY' => '595', 'QA' => '974', 'QN' => '374', 'QS' => '252', 'QY' => '90', 'RE' => '262', 'RO' => '40', 'RS' => '381', 'RU' => '7', 'RW' => '250', 'SA' => '966', 'SB' => '677', 'SC' => '248', 'SD' => '249', 'SE' => '46', 'SG' => '65', 'SH' => '290', 'SI' => '386', 'SJ' => '47', 'SK' => '421', 'SL' => '232', 'SM' => '378', 'SN' => '221', 'SO' => '252', 'SR' => '597', 'SS' => '211', 'ST' => '239', 'SV' => '503', 'SX' => '1721', 'SY' => '963', 'SZ' => '268', 'TA' => '290', 'TC' => '1649', 'TD' => '235', 'TG' => '228', 'TH' => '66', 'TJ' => '992', 'TK' => '690', 'TL' => '670', 'TM' => '993', 'TN' => '216', 'TO' => '676', 'TR' => '90', 'TT' => '1868', 'TV' => '688', 'TW' => '886', 'TZ' => '255', 'UA' => '380', 'UG' => '256', 'UK' => '44', 'US' => '1', 'UY' => '598', 'UZ' => '998', 'VA' => '379', 'VA' => '39', 'VC' => '1784', 'VE' => '58', 'VG' => '1284', 'VI' => '1340', 'VN' => '84', 'VU' => '678', 'WF' => '681', 'WS' => '685', 'XC' => '991', 'XD' => '888', 'XG' => '881', 'XL' => '883', 'XN' => '857', 'XN' => '858', 'XN' => '870', 'XP' => '878', 'XR' => '979', 'XS' => '808', 'XT' => '800', 'XV' => '882', 'YE' => '967', 'YT' => '262', 'ZA' => '27', 'ZM' => '260', 'ZW' => '263');

	return ($country == '') ? $countries : (isset($countries[$country]) ? $countries[$country] : '');
}

// Displays the update message
function zenziva_sms_upgrade() {
	global $zenziva_sms;

    echo '<div class="error fade" id="message"><h3>' . $zenziva_sms['plugin'] . '</h3><h4>' . sprintf(__("Please, set your %s. To make this plugin working properly", 'zenziva_sms'), '<a href="' . $zenziva_sms['ajustes'] . '" title="' . __('Settings', 'zenziva_sms') . '">' . __('settings', 'zenziva_sms') . '</a>') . '</h4></div>';
}

// Load stylesheets
function zenziva_sms_sample_message() {
	wp_register_style('zenziva_sms_css', plugins_url('style.css', __FILE__)); // Load stylesheets
	wp_register_style('zenziva_sms_sources', plugins_url('fonts/stylesheet.css', __FILE__)); // Loads the global style sheet
	wp_enqueue_style('zenziva_sms_sources'); // Loads the global style sheet

	$config = get_option('zenziva_sms_settings');
	if (!isset($config['gateway']) || !isset($config['userkey_zenziva']) || !isset($config['passkey_zenziva']) || !isset($config['admin_phone'])) add_action('admin_notices', 'zenziva_sms_upgrade'); // Check whether to show the update message
}
add_action('admin_init', 'zenziva_sms_sample_message');

// Remove all traces of the plugin to uninstall
function zenziva_sms_uninstall() {
  delete_option('zenziva_sms_settings');
}
register_deactivation_hook( __FILE__, 'zenziva_sms_uninstall' );
?>
