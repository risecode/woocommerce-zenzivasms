<?php global $zenziva_sms; ?>

<div class="wrap">
  <h2>
    <?php _e('Zenziva SMS Setting.', 'zenziva_sms'); ?>
  </h2>
  <?php
		settings_errors();
		$tab = 1;
		$configuracion = get_option('zenziva_sms_settings');
  ?>

  <p>
    <?php include('info-bar.php'); ?>
  </p>
  <form method="post" action="options.php">
    <?php settings_fields('zenziva_sms_settings_group'); ?>
    <table class="form-table">
      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[gateway]">
            <?php _e('SMS gateway:', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('Select your SMS gateway', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        	<td class="forminp forminp-number"><select id="zenziva_sms_settings[gateway]" name="zenziva_sms_settings[gateway]" tabindex="<?php echo $tab++; ?>">
            <?php
            $provider = array(
            							"reguler" => "SMS Reguler",
            							"center" => "SMS Center",
            							"masking" => "SMS Masking"
            							);
            foreach ($provider as $valor => $proveedor)
            {
							$chequea = '';
							if (isset($configuracion['gateway']) && $configuracion['gateway'] == $valor) $chequea = ' selected="selected"';
							echo '<option value="' . $valor . '"' . $chequea . '>' . $proveedor . '</option>' . PHP_EOL;
            }
            ?>
          </select></td>
      </tr>

      <!-- Zenziva -->
      <tr valign="top" class="zenziva">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[userkey_zenziva]">
            <?php _e('Userkey:', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php echo sprintf(__('The %s for your http api account.', 'zenziva_sms'), __('userkey', 'zenziva_sms'), "Zenziva"); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><input type="text" id="zenziva_sms_settings[userkey_zenziva]" name="zenziva_sms_settings[userkey_zenziva]" size="50" value="<?php echo (isset($configuracion['userkey_zenziva']) ? $configuracion['userkey_zenziva'] : ''); ?>" tabindex="<?php echo $tab++; ?>" /></td>
      </tr>
      <tr valign="top" class="zenziva">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[passkey_zenziva]">
            <?php _e('Passkey:', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php echo sprintf(__('The %s for your http api account.', 'zenziva_sms'), __('passkey', 'zenziva_sms'), "Zenziva"); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><input type="text" id="zenziva_sms_settings[passkey_zenziva]" name="zenziva_sms_settings[passkey_zenziva]" size="50" value="<?php echo (isset($configuracion['passkey_zenziva']) ? $configuracion['passkey_zenziva'] : ''); ?>" tabindex="<?php echo $tab++; ?>" /></td>
      </tr>
      <tr valign="top" class="center">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[subdomain_zenziva]">
            <?php _e('Subdomain:', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php echo sprintf(__('Your %s name.', 'zenziva_sms'), __('Subdomain', 'zenziva_sms'), "Zenziva"); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><input type="text" id="zenziva_sms_settings[subdomain_zenziva]" name="zenziva_sms_settings[subdomain_zenziva]" size="50" value="<?php echo (isset($configuracion['subdomain_zenziva']) ? $configuracion['subdomain_zenziva'] : ''); ?>" tabindex="<?php echo $tab++; ?>" /></td>
      </tr>

      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[admin_phone]">
            <?php _e('Your mobile number:', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('The mobile number registered in your SMS gateway account and where you receive the SMS messages', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><input type="text" id="zenziva_sms_settings[admin_phone]" name="zenziva_sms_settings[admin_phone]" size="50" value="<?php echo (isset($configuracion['admin_phone']) ? $configuracion['admin_phone'] : ''); ?>" tabindex="<?php echo $tab++; ?>" onkeyup="this.value = this.value.replace (/\D+/, '')" /></td>
      </tr>

      <!-- Variable: %id%, %order_key%, %billing_first_name%, %billing_last_name%, %billing_company%, %billing_address_1%, %billing_address_2%, %billing_city%, %billing_postcode%, %billing_country%, %billing_state%, %billing_email%, %billing_phone%, %shipping_first_name%, %shipping_last_name%, %shipping_company%, %shipping_address_1%, %shipping_address_2%, %shipping_city%, %shipping_postcode%, %shipping_country%, %shipping_state%, %shipping_method%, %shipping_method_title%, %payment_method%, %payment_method_title%, %order_subtotal%, %order_discount%, %cart_discount%, %order_tax%, %order_shipping%, %order_shipping_tax%, %order_total%, %status%, %shop_name% and %note% -->
      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[owner_neworder]">
            <?php _e('New Order Alert (For Admin):', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('Leave blank to disable this notification', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><textarea id="zenziva_sms_settings[owner_neworder]" name="zenziva_sms_settings[owner_neworder]" cols="50" rows="5" tabindex="<?php echo $tab++; ?>"><?php echo stripcslashes(isset($configuracion['owner_neworder']) ? $configuracion['owner_neworder'] : sprintf(__("Order No. %s received on ", 'zenziva_sms'), "%id%") . "%shop_name%" . "."); ?></textarea></td>
      </tr>
      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[customer_neworder]">
            <?php _e('New Order Alert (For Customer):', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('Leave blank to disable this notification', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><textarea id="zenziva_sms_settings[customer_neworder]" name="zenziva_sms_settings[customer_neworder]" cols="50" rows="5" tabindex="<?php echo $tab++; ?>"><?php echo stripcslashes(isset($configuracion['customer_neworder']) ? $configuracion['customer_neworder'] : sprintf(__('Your order No. %s is received on %s. Thank you for shopping with us!', 'zenziva_sms'), "%id%", "%shop_name%")); ?></textarea></td>
      </tr>
      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[order_processing]">
            <?php _e('Order processing (For Customer):', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('Leave blank to disable this notification', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><textarea id="zenziva_sms_settings[order_processing]" name="zenziva_sms_settings[order_processing]" cols="50" rows="5" tabindex="<?php echo $tab++; ?>"><?php echo stripcslashes(isset($configuracion['order_processing']) ? $configuracion['order_processing'] : sprintf(__('Thank you for shopping with us! Your order No. %s is now: ', 'zenziva_sms'), "%id%") . __('Processing', 'zenziva_sms') . "."); ?></textarea></td>
      </tr>
      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[order_completed]">
            <?php _e('Order completed (For Customer):', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('Leave blank to disable this notification', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><textarea id="zenziva_sms_settings[order_completed]" name="zenziva_sms_settings[order_completed]" cols="50" rows="5" tabindex="<?php echo $tab++; ?>"><?php echo stripcslashes(isset($configuracion['order_completed']) ? $configuracion['order_completed'] : sprintf(__('Thank you for shopping with us! Your order No. %s is now: ', 'zenziva_sms'), "%id%") . __('Completed', 'zenziva_sms') . "."); ?></textarea></td>
      </tr>
      <!--
      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[order_pending]">
            <?php _e('Order pending (For Customer):', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('Leave blank to disable this notification', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><textarea id="zenziva_sms_settings[order_pending]" name="zenziva_sms_settings[order_pending]" cols="50" rows="5" tabindex="<?php echo $tab++; ?>"><?php echo stripcslashes(isset($configuracion['order_pending']) ? $configuracion['order_pending'] : sprintf(__('Your order No. %s Status is: ', 'zenziva_sms'), "%id%") . __('Pending', 'zenziva_sms') . "."); ?></textarea></td>
      </tr>

      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[order_failed]">
            <?php _e('Order failed (For Customer):', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('Leave blank to disable this notification', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><textarea id="zenziva_sms_settings[order_failed]" name="zenziva_sms_settings[order_failed]" cols="50" rows="5" tabindex="<?php echo $tab++; ?>"><?php echo stripcslashes(isset($configuracion['order_failed']) ? $configuracion['order_failed'] : sprintf(__('Your order No. %s Status is: ', 'zenziva_sms'), "%id%") . __('Failed', 'zenziva_sms') . "."); ?></textarea></td>
      </tr>

      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[order_refunded]">
            <?php _e('Order refunded (For Customer):', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('Leave blank to disable this notification', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><textarea id="zenziva_sms_settings[order_refunded]" name="zenziva_sms_settings[order_refunded]" cols="50" rows="5" tabindex="<?php echo $tab++; ?>"><?php echo stripcslashes(isset($configuracion['order_refunded']) ? $configuracion['order_refunded'] : sprintf(__('Your order No. %s Status is: ', 'zenziva_sms'), "%id%") . __('Refunded', 'zenziva_sms') . "."); ?></textarea></td>
      </tr>

      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[order_cancelled]">
            <?php _e('Order cancelled (For Customer):', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('Leave blank to disable this notification', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><textarea id="zenziva_sms_settings[order_cancelled]" name="zenziva_sms_settings[order_cancelled]" cols="50" rows="5" tabindex="<?php echo $tab++; ?>"><?php echo stripcslashes(isset($configuracion['order_cancelled']) ? $configuracion['order_cancelled'] : sprintf(__('Your order No. %s Status is: ', 'zenziva_sms'), "%id%") . __('Cancelled', 'zenziva_sms') . "."); ?></textarea></td>
      </tr>
    -->
      <tr valign="top">
        <th scope="row" class="titledesc"> <label for="zenziva_sms_settings[order_note]">
            <?php _e('Notes custom message:', 'zenziva_sms'); ?>
          </label>
          <img class="help_tip" data-tip="<?php _e('Leave blank to disable this notification', 'zenziva_sms'); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png');?>" height="16" width="16" /> </th>
        <td class="forminp forminp-number"><textarea id="zenziva_sms_settings[order_note]" name="zenziva_sms_settings[order_note]" cols="50" rows="5" tabindex="<?php echo $tab++; ?>"><?php echo stripcslashes(isset($configuracion['order_note']) ? $configuracion['order_note'] : sprintf(__('A note has just been added to your order No. %s: ', 'zenziva_sms'), "%id%") . "%note%"); ?></textarea></td>
      </tr>
    </table>
    <p class="submit">
      <input class="button-primary" type="submit" value="<?php _e('Save Changes', 'zenziva_sms'); ?>"  name="submit" id="submit" tabindex="<?php echo $tab++; ?>" />
    </p>
  </form>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$('select').on('change', function () {
		control($(this).val());
	});

	var control = function(capa) {
		var proveedores= new Array();
		<?php foreach($provider as $indice => $valor) echo "proveedores['$indice'] = '$valor';" . PHP_EOL; ?>

		for (var valor in proveedores) {
    		if (valor == capa) $('.' + capa).show();
			else $('.' + valor).hide();
		}
	};
	control($('select').val());
});
</script>
